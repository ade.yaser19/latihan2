const express = require("express");
require ("dotenv").config();

const PORT = process.env.PORT || 5000;

const router = require("./routes/route");

const middelwareLogRequest = require("./middleware/log");
const upload = require('./middleware/muter');

const app = express();
app.use(middelwareLogRequest);
app.use(express.json());
app.use('/assets',express.static('public/image'));
app.post('/upload',upload.single('file'),(req, res) => {
    res.json({
        message: 'Upload berhasil'
    })
})



app.use('/',router);
app.use('/users',router);


app.listen(PORT,()=>{
    console.log(`Server Berhasil Di Running di Port ${PORT}`);
});


